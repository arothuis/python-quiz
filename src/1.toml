title = "Data Types"

[[questions]]
description = """
What is the type of `value`?

```python
value = "Welcome!"
```
"""

options = ["String", "Integer", "Boolean", "Float"]
answer = "String"
tip = """
The quotation marks (`"` or `'`) indicate a certain data type
that contains a sequence of characters to represent
text.
"""


[[questions]]
description = """
What is the type of `value`?

```python
value = 1 + 2.0
```
"""

options = ["TypeError", "Integer", "Boolean", "Float"]
answer = "Float"
tip = """
We are adding an integer and a float.
In order to make one type out of two,
Python can implicitly convert them when
they're numbers. When doing so, it will
convert to the widest, the most imprecise one 
of both types.
"""


[[questions]]
description = """
What is the type of `value`?

```python
value = ["Hello world"]
```
"""

options = ["String", "List", "Dictionary", "Float"]
answer = "List"
tip = """
The straight brackets (`[` and `]`) indicate
that we are dealing with a certain ordered, mutable
sequence of values.
"""


[[questions]]
description = """
What is the type of `value`?

```python
value = "True"
```
"""

options = ["Boolean", "String", "List", "Integer"]
answer = "String"
tip = """
Do not be fooled by its contents. We can see by the quotation marks
(`"` or `'`) that we are dealing with a data type that deals with
text.
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = True + 1.0
```
"""

options = ["Float", "Integer", "Boolean", "TypeError"]
answer = "Float"
tip = """
We are adding a boolean and a float.
In order to make one type out of two,
Python can implicitly convert them when
they're numbers or can behave like numbers. 
When doing so, it will
convert to the widest, the most imprecise one 
of both types.
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = True + 4
```
"""

options = ["Float", "Integer", "Boolean", "TypeError"]
answer = "Integer"
tip = """
We are adding a boolean and an integer.
In order to make one type out of two,
Python can implicitly convert them when
they're numbers or can behave like numbers. 
When doing so, it will
convert to the widest, the most imprecise one 
of both types.
"""


[[questions]]
description = """
What is the type of `value`?

```python
value = 4 / 2
```
"""

options = ["Float", "Integer", "Boolean", "TypeError"]
answer = "Float"
tip = """
Division will always result in a decimal value in order
to always give a consistent result; not every division fits.
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = {"Hello", "Goodbye"}
```
"""

options = ["List", "String", "Dictionary", "Set"]
answer = "Set"
tip = """
The comma indicates we are dealing with a collection
of some sort. The curly braces (`{` and `}`) tell us that it is
not a list or a tuple. It cannot be a dictionary,
because the keys and corresponding values would be
separated by a colon (`:`).
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = [1, 2, 150, (5, 2)]
```
"""

options = ["List", "String", "Tuple", "Set"]
answer = "List"
tip = """
Collections can contain values of all kinds of types
and the types can even be mixed. When determining the type,
always look at the outermost characters. In this case,
these are straight brackets (`[` and `]`).
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = [{"name": "Adrian", "age": 42}]
```
"""

options = ["List", "Tuple", "Dictionary", "Set"]
answer = "List"
tip = """
Collections can contain values of all kinds of types
and the types can even be mixed. When determining the type,
always look at the outermost characters. In this case,
these are straight brackets (`[` and `]`).
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = {"city": "Amsterdam", "crowded": True}
```
"""

options = ["List", "Tuple", "Dictionary", "Set"]
answer = "Dictionary"
tip = """
The curly braces (`{` and `}`) tell us that it is
not a list or a tuple. Note that some values are 
separated by a colon (`:`). This creates a mapping
of some sort.
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = "1" + "2"
```
"""

options = ["String", "TypeError", "Integer", "List"]
answer = "String"
tip = """
Don't get fooled by the plus sign. When dealing with
two strings like here, it means something different 
than when dealing with two numbers. In this case
it means **concatenation**.
"""



[[questions]]
description = """
What is the type of `value`?

```python
value = ["1"] + "2"
```
"""

options = ["String", "TypeError", "Integer", "List"]
answer = "TypeError"
tip = """
Lists can be concatenated with other lists.
Strings with other strings.
Lists and strings can, however, not be concatenated
with eachother. Adding something to a list is done
with the `.append()` method.
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = ['1'] + []
```
"""

options = ["String", "TypeError", "Integer", "List"]
answer = "List"
tip = """
Lists can be concatenated with other lists.
The elements of the second list are appended
to the first list.
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = {"name": "Gerry"} + {"age": 22}
```
"""

options = ["String", "TypeError", "Integer", "List"]
answer = "TypeError"
tip = """
Dictionaries are **unordered**. Therefore, they
cannot be concatenated. To add something to a dictionary,
use element assignment: `dict[key] = value`.
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = (1, 2) + (3, 4)
```
"""

options = ["Float", "TypeError", "Integer", "Tuple"]
answer = "Tuple"
tip = """
Although tuples are immutable,
tuples can be concatenated into a **new** tuple.
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = 1, 2
```
"""

options = ["String", "TypeError", "Integer", "Tuple"]
answer = "Tuple"
tip = """
In some cases, it is more readable to remove the
parentheses, `(` and `)`, when dealing with this
tightly related, ordered and immutable collection
of values. However, in most cases, you would write this as
`(1, 2)` to prevent confusion.
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = (2)
```
"""

options = ["String", "TypeError", "Integer", "Tuple"]
answer = "Integer"
tip = """
Parentheses, `(` and `)`, are used to make sure
certain expressions are evaluated before others.
In this case, the parentheses only wrap an
integer, evaluating to an integer.
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = True + False
```
"""

options = ["Boolean", "TypeError", "Integer", "Tuple"]
answer = "Integer"
tip = """
Addition always returns a number type: an integer or a float.
In this case, because we are dealing with two booleans, we can
see them as a `1` (for `True`) and a `0` (for `False`).
Python implicitly converts this to `1 + 0`.
"""

[[questions]]
description = """
What is the type of `value`?

```python
value = True and 0.0
```
"""

options = ["Boolean", "TypeError", "Integer", "Float"]
answer = "Float"
tip = """
We are dealing with a boolean and a float. 
Python can implicitly convert them. 
When doing so, it will
convert to the widest, the most imprecise one 
of both types.
"""