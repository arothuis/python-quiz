title = "Operators"

[[questions]]
description = """
What is printed to the screen?

```python
value = 2 + 1.5
print(value)
```
"""

options = ["3", "3.5", "TypeError", "4"]
answer = "3.5"
tip = """
Adding a float to an integer will result in a
float.
"""

[[questions]]
description = """
What is printed to the screen?

```python
value = "1" + "2"
print(value)
```
"""

options = ["3", "12", "TypeError", "1"]
answer = "12"
tip = """
Note that we are not performing addition.
The plus-operator is considered to mean
concatenation when two strings are its operands.
"""

[[questions]]
description = """
What is printed to the screen?

```python
value = "2" + 5
print(value)
```
"""

options = ["25", "7", "TypeError", "2"]
answer = "TypeError"
tip = """
The plus operator can mean concatenation when
dealing with two strings or addition when dealing
with two numbers (floats, integers) or number-like types (booleans).

In this case, we have a string and an integer. Python does not
know what to do.
"""


[[questions]]
description = """
What is printed to the screen?

```python
value = 6 / 2
print(value)
```
"""

options = ["3.0", "3", "0", "4"]
answer = "3.0"
tip = """
Division will always result in a decimal; a float.
Not every division will fit perfectly
and will result in a whole number.
"""

[[questions]]
description = """
What is printed to the screen?

```python 
value = 5 // 2
print(value)
```
"""

options = ["2.0", "2.5", "2", "3"]
answer = "2"
tip = """
When performing floor division (`//`),
the left-hand side (`5`) is first divided
by the right-hand side (`2`) and then "floored"
(rounded down). 

In other words, how many exact times
does 2 fit in 5?
print(value)
"""

[[questions]]
description = """
What is printed to the screen?

```python
value = 1 // 2
print(value)
```
"""

options = ["0.5", "1", "2", "0"]
answer = "0"
tip = """
When performing floor division (`//`),
the left-hand side (`1`) is first divided
by the right-hand side (`2`) and then "floored"
(rounded down). 

In other words, how many exact times
does 2 fit in 1?
"""


[[questions]]
description = """
What is printed to the screen?

```python
value = 2 % 4
print(value)
```
"""

options = ["0.5", "1", "2", "0"]
answer = "2"
tip = """
When performing a modulo or remainder operation,
we calculate how much remains after calculating
how many times the right-hand side (`4`) fits
in the left-hand side (`2`) exactly.
In other words: `2 % 4` is the same as `2 - (2 // 4)`.

In the case of `%`, whenever the left-hand side 
is smaller than the right-hand side, it always 
returns the left-hand side.
"""


[[questions]]
description = """
What is printed to the screen?

```python
value = 1 and True
print(value)
```
"""

options = ["True", "False", "TypeError", "1"]
answer = "True"
tip = """
An `and` between two truthy values results in `True`.
"""

[[questions]]
description = """
What is printed to the screen?

```python
value = not 2 or not True
print(value)
```
"""

options = ["True", "False", "TypeError", "1"]
answer = "False"
tip = """
Any number other than 0 will be seen as True when
working with a boolean operator (`and`, `or`, `not`).

This means `not 2` evaluates to `not True`, which means `False`.
`False and not True` evaluates to `False`.
"""


[[questions]]
description = """
What is printed to the screen?

```python
value = True and False or True
print(value)
```
"""

options = ["True", "False", "TypeError", "1"]
answer = "True"
tip = """
An `and` between two truthy values results in `True`.

An `or` with a single truthy value results in `True`.

Python first evaluates `False or True`, returning `True`.
Afterwards, Python evaluates `True and True`.
In other words: `value = True and (False or True)`.
"""


[[questions]]
description = """
What is printed to the screen?

```python
value = [1, 2, 3] + [5, 4, 3]
print(value)
```
"""

options = [
    "[1, 2, 3, 4, 5]",
    "[1, 2, 3, 3, 4, 5]",
    "[1, 2, 3, 5, 4, 3]",
    "TypeError", 
]
answer = "[1, 2, 3, 5, 4, 3]"
tip = """
Concatenation between two lists results in
the elements of the right-hand list being
appended to the left-hand list.
"""

[[questions]]
description = """
What is printed to the screen?

```python
word_a = "Hello"
word_b = "World"
value = word_a + word_b
print(value)
```
"""

options = [
    "Hello World",
    "['Hello', 'World']",
    "TypeError", 
    "HelloWorld"
]
answer = "HelloWorld"
tip = """
Concatenation of strings (`+`) is performed without
regard to the content of the strings. Therefore,
Python does not add any extra whitespace.

If we wanted to write "Hello World", the concatenation
would look different:
`value = word_a + " " + word_b`
"""

[[questions]]
description = """
What is printed to the screen?

```python
word_a = "Apartment"
word_b = "City"
value = word_a > word_b
print(value)
```
"""

options = [
    "True",
    "False",
    "TypeError", 
    "None"
]
answer = "False"
tip = """
We are using a comparison operator, greater than (`>`). 

Note that we are using it on strings here. This means the
strings are converted to their numeric (unicode or ASCII) value.
This results in an alphabetic comparison: 
does "apartment" **come after** "city" in the dictionary?

For a comparison by length, we would have to take the length of the
strings using the `len()` function: `len(word_a) > len(word_b)`.
"""

[[questions]]
description = """
What is printed to the screen?

```python
coordinate = (0, 5)
coords = [(0, 0), (0, 3)]
value = coords + coordinate
print(value)
```
"""

options = [
    "[(0, 0), (0, 3)]",
    "[(0, 0), (0, 3), (0, 5)]",
    "TypeError", 
    "[(0, 0), (0, 3), 0, 5]"
]
answer = "TypeError"
tip = """
In this case, an attempt is made to concatenate a tuple
to the list. 

We can only concatenate a list to another list.

If we wanted to add the tuple `(0, 5)` to our existing list
of tuples, we would perform `coords.append((0, 5))` and print
the value of `coords`.
"""


[[questions]]
description = """
What is printed to the screen?

```python
greeting = "Hi"
greeting[2] = "y"
greeting[3] = "a"
print(greeting)
```
"""

options = [
    "Hia",
    "Hiya",
    "TypeError", 
    "a"
]
answer = "TypeError"
tip = """
Unlike lists, strings are **immutable**.
Their value cannot be changed!
"""

[[questions]]
description = """
What is printed to the screen?

```python
greetings = ["Hi", "Hello"]
greetings[0] = "Good day" 
greetings[1] = "Good day"
print(greetings)
```
"""

options = [
    "Good day",
    "['Good day']",
    "TypeError", 
    "['Good day', 'Good day']"
]
answer = "['Good day', 'Good day']"
tip = """
Lists are **mutable**. They can be changed after creation.
Here, we use the element assignment operator.
"""

[[questions]]
description = """
What is printed to the screen?

```python
cities = ["Amsterdam", "Utrecht"]
cities[0] = cities[1] 
cities[1] = cities[0]
print(cities)
```
"""

options = [
    "['Utrecht', 'Utrecht']",
    "['Utrecht', 'Amsterdam']",
    "['Amsterdam', 'Utrecht']",
    "TypeError", 
]
answer = "['Utrecht', 'Utrecht']"
tip = """
Lists are **mutable**. They can be changed after creation.
Here, we use the element assignment operator after getting
a value using the list access operator.

We first set the first element to be the value of 'Utrecht'.
Then we set the second element to be of the value of the (new)
first value: 'Utrecht'.

If we wanted to swap values, we would do the following:
`cities[0], cities[1] = cities[1], cities[0]`
"""


[[questions]]
description = """
What is printed to the screen?

```python
lunch = ("cookies", "milk")
food, drink = lunch
print(drink)
```
"""

options = [
    "('cookies', 'milk')",
    "milk",
    "TypeError", 
    "cookies",
]
answer = "milk"
tip = """
We can unpack tuples and lists and assign the values to individual variables,
because they are **ordered**. Python knows which variable should go where.
"""

[[questions]]
description = """
What is printed to the screen?

```python
value = {1, 2, 3} + {2, 1, 3}
```
"""

options = [
    "{1, 2, 3, 2, 1, 3}",
    "{1, 2, 3}",
    "TypeError", 
    "[{1, 2, 3}, {2, 1, 3}]",
]
answer = "TypeError"
tip = """
Sets cannot be concatenated, because they are unordered.
If we wanted to join them together, we would do it like this:
`{1, 2, 3}.union({2, 1, 3})`. This would result in `{1, 2, 3}`
as sets contain no duplicates (their elements are **unique**).
"""


[[questions]]
description = """
What is printed to the screen?

```python
my_friends = {"Obi-wan", "Yoda"}
your_friends = {"Luke", "Obi-wan", "Palpatine"}

party = my_friends.intersection(your_friends)

print(party)
```
"""

options = [
    "{'Obi-wan', 'Yoda', 'Luke', 'Palpatine'}",
    "{'Luke', 'Obi-wan', 'Palpatine'}",
    "{'Obi-wan', 'Yoda'}", 
    "{'Obi-wan'}",
]
answer = "{'Obi-wan'}"
tip = """
An intersection of two sets results in a set containing
the elements both have in common.
In other words, the new set will only contain 
the elements that are present in both sets.
"""